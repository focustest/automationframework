package testcases;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import resources.Base;
import pageobjects.AccountsPage;
import pageobjects.LandingPage;
import pageobjects.LoginPage;

import java.io.IOException;


public class LoginTest extends Base {
    Logger logger;
    WebDriver driver;

  @Test(dataProvider = "getLoginData" )
    public void login(String email_dp,String password_dp, String expresult) throws IOException, InterruptedException {
      //logger = LogManager.getLogger(LoginTest.class.getName());
        LandingPage landingPage = new LandingPage(driver);
      logger.debug("opened browser.nnnnlll.");
        landingPage.myAccountDropdown().click();
        landingPage.loginOption().click();
      logger.debug("clicked on login button.");
        Thread.sleep(3000);

        LoginPage loginPage = new LoginPage(driver);
        loginPage.emailAddressTextField().sendKeys(email_dp);
        loginPage.passwordField().sendKeys(password_dp);
        loginPage.loginButton().click();

        AccountsPage accountPage = new AccountsPage(driver);
      String actualResult;
      try{
          System.out.println(accountPage.editYourAccountInformation().isDisplayed());
          actualResult="Success";
          logger.info("Test passed");
          takeScreenshot("LoginTests-Passed", driver);

      }catch (Exception e){
       actualResult="failure";
       logger.error("Test Failed");
          takeScreenshot("LoginTests-Failed", driver);
      }

      Assert.assertEquals(expresult,actualResult);
       // Assert.assertTrue(accountPage.editYourAccountInformation().isDisplayed());

    }

    @BeforeMethod
    public void openApplication() throws IOException {
        logger = LogManager.getLogger(LoginTest.class.getName());
        driver=initializeBrowser();
        driver.get(prop.getProperty("url"));
        logger.info("Browser opened");
      //
    }
    @AfterMethod
    public void closure()
    {logger.info("Closed..");
        driver.close();
    }

    @DataProvider
    public Object[][] getLoginData(){
        Object[][] data1 = {{"js123@gmail.com","test1234","Success"}};
        return data1;
    }

}
