package testcases;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import resources.Base;

import java.io.IOException;

public class TestThree extends Base {
    WebDriver driver;
    @Test
    public void testThree() throws IOException, InterruptedException {
        driver = initializeBrowser();
        driver.get(prop.getProperty("url"));
        System.out.println("Testi,,,,ng 2 completed-2-");
        Thread.sleep(2000);
        driver.close();
        System.out.println("Testing 3 completed--3--");
    }
}
