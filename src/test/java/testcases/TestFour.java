package testcases;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import resources.Base;

import java.io.IOException;

public class TestFour extends Base {
    public WebDriver driver;
    @Test
    public void testFour() throws IOException, InterruptedException {
        driver = initializeBrowser();
        driver.get(prop.getProperty("url"));
        System.out.println("Testing 4 completed-2-");
        Thread.sleep(2000);
        Assert.assertTrue(false);
        driver.close();
        System.out.println("Testing 4 completed--4-");
    }



    public void closingBrowser(){
        driver.close();
    }
}
